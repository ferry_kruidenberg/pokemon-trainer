export interface PokeApiModel{
    count: number,
    next: string | null,
    previous: string | null
    results: PokeApiModelResults[]
}

export interface PokeApiModelResults{
    name: string,
    url: string
}
