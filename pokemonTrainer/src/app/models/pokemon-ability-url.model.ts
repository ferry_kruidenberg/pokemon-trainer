export interface PokemonAbilityUrlModel{
    effect_entries: effect[]

}

export interface effect{
    effect: string,
    language: {
        name: string
    }
    short_effect: string
}
