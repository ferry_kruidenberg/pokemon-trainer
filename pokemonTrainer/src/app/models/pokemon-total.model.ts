export interface PokemonTotalModel{
    name: string
    abilities: PokemonAbilities[],
    height: number,
    id: number,
    stats: stats[]
    types: type[]
    species: {
        name: string,
        url: string
    }
    moves: move[]


}

export interface PokemonAbilities{
    ability: PokemonAbility
    is_hidden: boolean
}

export interface PokemonAbility {
    name: string,
    url: string,
}

export interface stats{
    base_stat: number
    effort: number
    stat: stat

}

export interface stat{
    name: string
    url: string
}

export interface type{
    slot: number,
    type:{
        name: string
    }
}

export interface move{
    move: {
        name: string
    }
}
