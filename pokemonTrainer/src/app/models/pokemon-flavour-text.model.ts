export interface PokemonFlavourTextModel {
    flavor_text_entries: flavor_text[]
}

export interface flavor_text {
    flavor_text: string,
    language:{
        name: string
    }
}
