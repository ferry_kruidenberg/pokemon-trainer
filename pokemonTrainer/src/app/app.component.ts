import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
        <app-navbar-component></app-navbar-component>
    <router-outlet></router-outlet>
    `
})
export class AppComponent {
    title = 'pokemonTrainer';
}
