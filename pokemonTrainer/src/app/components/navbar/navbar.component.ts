import {Component} from "@angular/core";
import {SessionService} from "../../services/session/session.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-navbar-component',
    templateUrl: './navbar.component.html'
})
export class NavbarComponent{
    constructor(private readonly sessionService : SessionService,
        private readonly router: Router) {
    }
    logout(){
        this.sessionService.logout();
        this.router.navigate([""]);
    }

    checkForUser(){
        if(this.sessionService.user){
            return true;
        }
        return false;
    }

    catalogueButton(){
        this.router.navigate(["Catalogue/1"])
    }

    profileButton(){
        this.router.navigate(["profile"]);
    }
}
