import {Component, OnInit} from "@angular/core";
import {SessionService} from "../../services/session/session.service";
import {User} from "../../models/user.model";
import {Pokemon} from "../../models/pokemon.model";
import {Router} from "@angular/router";
import {LoginService} from "../../services/login/login.service";

@Component({
    selector: 'app-profile-component',
    templateUrl: './profile.component.html',
    styleUrls: ['profile.component.css']
})
export class ProfileComponent{
    constructor(private readonly sessionService : SessionService,
                private readonly router: Router,
                private readonly loginService: LoginService) {
    }

    user : User | undefined;
    pokemonArray : Pokemon[] | undefined;

    ngOnInit() {
        const localUser = localStorage.getItem("user");
        if(localUser !== null){
            //re-gets the user data from the database before showing the profile page
            this.loginService.authenticate((JSON.parse(localUser) as User).username, async (user: User) => {
                this.sessionService.setUser(user);
                this.user = this.sessionService.user;
                    this.pokemonArray = user.pokemon;
            });
        }
    }

    click(index: number) {
        if (this.pokemonArray) {
            this.router.navigate([`/pokemon/${this.pokemonArray[index].id}`]);
        }
    }
}
