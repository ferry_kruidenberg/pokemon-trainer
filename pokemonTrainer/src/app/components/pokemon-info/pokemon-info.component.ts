import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {ActivatedRoute, NavigationStart, Router} from "@angular/router";
import {Location} from '@angular/common'
import {PokemonDetailService} from "../../services/pokemon-detail/pokemon-detail.service";
import {PokemonTotalModel} from "../../models/pokemon-total.model";
import {environment} from "../../../environments/environment";
import {SessionService} from "../../services/session/session.service";
import {Pokemon} from "../../models/pokemon.model";
import {User} from "../../models/user.model";
import {filter} from "rxjs/operators";


@Component({
    selector: 'app-catalogue-info',
    templateUrl: 'pokemon-info.component.html',
    styleUrls: ['pokemon-info.component.css']
})
export class PokemonInfoComponent implements OnInit {
    pokeSpriteURL: string = environment.pokeSpriteURL;
    pokeAPIURL: string = environment.pokeAPIURL;
    maxStats = [255, 190, 250, 194, 250, 200] // [hp, attack, defence, sp-attack, sp-defence, speed]
    color: any = {
        normal: "#A8A77A",
        fire: "#EE8130",
        water: "#63a9f0",
        electric: "#F7D02C",
        grass: "#4cc7b0",
        ice: "#96D9D6",
        fighting: "#C22E28",
        poison: "#A33EA1",
        ground: "#E2BF65",
        flying: "#A98FF3",
        psychic: "#F95587",
        bug: "#A6B91A",
        rock: "#B6A136",
        ghost: "#735797",
        dragon: "#6F35FC",
        dark: "#705746",
        steel: "#B7B7CE",
        fairy: "#D685AD"
    }

    user: User | undefined
    hasPokemon: boolean = false
    pokemonStats: PokemonTotalModel | undefined
    public pokemonId: number = 0;
    pokemonModel: Observable<PokemonTotalModel> | undefined
    pokemonAbility: string[] = []
    flavourText: string = ''
    showAllMoves: boolean = false
    showButtonName: string = "Show more"


    constructor(
        private readonly currentRoute: ActivatedRoute,
        private readonly location: Location,
        private readonly pokemonDetailService: PokemonDetailService,
        private readonly sessionService: SessionService) {

    }


    setAbilities(pokemonStats: PokemonTotalModel) {
        pokemonStats.abilities.map((ability) => {
            return this.pokemonDetailService.getAbilityText(ability.ability.url).subscribe(
                (text) => {
                    this.pokemonAbility.push(text)
                }
            )
        })
    }

    setFlavourText(pokemonStats: PokemonTotalModel) {
        this.pokemonDetailService.getFlavourText(pokemonStats.species.url).subscribe(
            (flavourText) => {
                //there is as \f: form feed in the data and windows can't display that properly
                this.flavourText = flavourText.flavor_text.replace('\f', ' ')
            }
        )

    }


    ngOnInit() {
        this.user = this.sessionService.user
        this.currentRoute.params.subscribe(({id}) => {
            this.pokemonId = id;
            this.pokemonModel = this.pokemonDetailService.fetchPokemon(id);
        });
        this.pokemonModel?.subscribe(
            (pokemonObject: PokemonTotalModel) => {
                this.pokemonStats = pokemonObject
                this.setAbilities(this.pokemonStats)
                this.setFlavourText(this.pokemonStats)
            }
        )
        this.hasPokemon = Boolean(this.user?.pokemon.filter(element => element.id === this.pokemonId).length)
    }


    showHideMoves() {
        this.showAllMoves = !this.showAllMoves
        if (this.showAllMoves) {
            this.showButtonName = "Show less"
        } else {
            this.showButtonName = "Show more"
        }
    }

    catchPokemon() {
        if (this.pokemonStats !== undefined) {
            this.user = this.sessionService.user
            const pokemonItem: Pokemon = {
                id: this.pokemonId,
                name: this.pokemonStats?.name,
                url: `${this.pokeAPIURL}/pokemon/${this.pokemonId}`,
                image: `${this.pokeSpriteURL}/${this.pokemonId}.png`
            }
            this.user?.pokemon.push(pokemonItem)

            this.sessionService.setUser(this.user)
            this.hasPokemon = true

        }

    }

    releasePokemon(){
        if(this.pokemonStats !== undefined){
            this.user = this.sessionService.user;
            this.user?.pokemon.forEach((pokemonDbItem,index)=>{
                if(pokemonDbItem.id===this.pokemonId) this.user?.pokemon.splice(index,1);
            });

            this.sessionService.setUser(this.user);
            this.hasPokemon = false;

        }
    }

}
