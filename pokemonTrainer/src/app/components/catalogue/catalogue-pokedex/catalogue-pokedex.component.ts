import {Component, OnInit} from "@angular/core";
import {SessionService} from "../../../services/session/session.service";
import {Pokemon} from "../../../models/pokemon.model";
import {Observable} from "rxjs";
import {ActivatedRoute, NavigationEnd, NavigationStart, Router} from "@angular/router";
import { Location } from '@angular/common'
import {User} from "../../../models/user.model";
import {LoginService} from "../../../services/login/login.service";


@Component({
    selector: 'app-catalogue-pokedex',
    templateUrl: 'catalogue-pokedex.component.html',
    styleUrls: ['catalogue-pokedex.component.css']
})
export class cataloguePokedexComponent implements OnInit{
    index = 1
    itemPerPage = 30
    maxPage = 0
    pokemonArray: Observable<Pokemon[]> | undefined
    ownedPokemonArray: Pokemon[] | undefined;

    constructor(private readonly sessionService: SessionService,
                private readonly router: Router,
                private readonly currentRoute: ActivatedRoute,
                private readonly location: Location,
                private readonly loginService : LoginService) {

        router.events.subscribe(val => {
            if(val instanceof NavigationEnd && val.url.includes('catalogue')) {
                this.index = Number(val.url.split('/').pop())
                this.initPokedex()
            }
        });

    }

    initPokedex(){
        this.sessionService.numberOfPokemon.subscribe(
            (count: number) => {
                this.maxPage = Math.ceil(count/this.itemPerPage)
                if(isNaN(+this.index)){
                    this.router.navigate([`/catalogue/1`])
                }
                if (this.index > this.maxPage){
                    this.index = this.maxPage
                    this.router.navigate([`/catalogue/${this.maxPage}`])
                }
                if (this.index < 1){
                    this.index = 1
                    this.router.navigate([`/catalogue/1`])
                }
                this.pokemonArray = this.sessionService.getPokemon((this.index - 1) * this.itemPerPage,this.itemPerPage)
            }
        )
    }

    ngOnInit() {
        this.currentRoute.params.subscribe(({id}) => {
            this.index = id;
        });
        const localUser = localStorage.getItem("user");
        if(localUser !== null){
            //re-gets the user data from the database before showing the profile page
            this.loginService.authenticate((JSON.parse(localUser) as User).username, async (user: User) => {
                this.sessionService.setUser(user);
                this.ownedPokemonArray = user.pokemon;
            });
        }

        this.initPokedex()
    }

    checkIfOwnedPokemon(id: number) : boolean{
        if(this.ownedPokemonArray !== undefined){
            for(let i = 0; i<this.ownedPokemonArray?.length; i++){
                if(this.ownedPokemonArray[i].id == id){
                    return true;
                }
            }
        }
        return false;
    }


    //Goto previous catalogue page
    previous(){
        this.index--
        this.router.navigate([`catalogue/${this.index}`])
        this.pokemonArray = this.sessionService.getPokemon((this.index - 1) * this.itemPerPage,this.itemPerPage)
    }

    //Goto next catalogue page
    next(){
        this.index++
        this.router.navigate([`catalogue/${this.index}`])
        this.pokemonArray = this.sessionService.getPokemon((this.index - 1) * this.itemPerPage,this.itemPerPage)
    }


    click(index: number){
        this.pokemonArray?.subscribe(
            (pokeItem: Pokemon[]) => {
                this.router.navigate([`/pokemon/${pokeItem[index].id}`])
            }
        )

    }

}
