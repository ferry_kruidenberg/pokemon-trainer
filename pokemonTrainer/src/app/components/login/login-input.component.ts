import {Component} from "@angular/core";
import {LoginService} from "../../services/login/login.service";
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";
import {SessionService} from "../../services/session/session.service";
import {User} from "../../models/user.model";

@Component({
    selector: 'app-login-input',
    templateUrl: './login-input.component.html'
})
export class LoginInputComponent {
    constructor(private readonly loginService: LoginService,
                private readonly sessionService: SessionService,
                private readonly router: Router) {
    }

    onSubmit(loginForm: NgForm) {
        this.loginService.authenticate(loginForm.value.username, async (user: User) => {
            this.sessionService.setUser(user)
            await this.router.navigate(['catalogue/1'])
        });


    }
}
