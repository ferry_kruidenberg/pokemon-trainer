import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {LoginPage} from "./pages/login/login.page";
import {AppRoutingModule} from "./app-routing.module";
import {ProfilePage} from "./pages/profile/profile.page";
import {CataloguePage} from "./pages/catalogue/catalogue.page";
import {LoginInputComponent} from "./components/login/login-input.component";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {ContainerComponent} from "./components/container/container.component";
import {cataloguePokedexComponent} from "./components/catalogue/catalogue-pokedex/catalogue-pokedex.component";
import {PokemonInfoComponent} from "./components/pokemon-info/pokemon-info.component";
import {PokemonInfoPage} from "./pages/pokemon-info/pokemon-info.page";
import {ProfileComponent} from "./components/profile/profile.component";
import {NavbarComponent} from "./components/navbar/navbar.component";

@NgModule({
    declarations: [
        AppComponent,
        ContainerComponent,
        LoginPage,
        LoginInputComponent,
        ProfilePage,
        CataloguePage,
        cataloguePokedexComponent,
        PokemonInfoComponent,
        PokemonInfoPage,
        ProfileComponent,
        NavbarComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
