import {Component} from "@angular/core";
import {Location} from "@angular/common";

@Component({
    selector: 'app-pokemon-info',
    templateUrl: './pokemon-info.page.html',
    styleUrls: ['./pokemon-info.page.css']
})
export class PokemonInfoPage {
    constructor(private readonly location: Location) {
    }

    back(){
        this.location.back()
    }
}
