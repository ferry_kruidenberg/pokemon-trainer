import { Component, OnInit } from '@angular/core'
import {Router} from "@angular/router";
import {SessionService} from "../../services/session/session.service";
import {User} from "../../models/user.model";
import {LoginService} from "../../services/login/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit{
    constructor(private readonly router: Router,
                private readonly sessionService: SessionService,
                private readonly loginService: LoginService) {
    }

    ngOnInit() {
        //Checks if the user already exists
        if (this.sessionService.user !== undefined){
            this.router.navigate(['catalogue/1'])
        }
        else if (localStorage.getItem("user")) {
            this.loginService.findByUsername((JSON.parse(localStorage.getItem("user")!) as User).username).subscribe(
                (users: User[]) => {
                    if (users.length) {
                        this.sessionService.setUser(users[0])
                        this.router.navigate(['catalogue/1'])
                    }
                }
            )
        }
    }
}
