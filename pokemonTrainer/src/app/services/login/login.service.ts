import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable, of} from "rxjs";
import {User} from "../../models/user.model";
import {switchMap} from "rxjs/operators";
import {SessionService} from "../session/session.service";

const API_URL = environment.apiURL;
const API_KEY = environment.apiKey;

@Injectable({
    providedIn: "root"
})
export class LoginService{
    constructor(private readonly  http : HttpClient) {
    }

    findByUsername(username : string) : Observable<User[]>{
        return this.http.get<User[]>(`${API_URL}/trainers?username=${username}`)
    }

    private createUser(username : string) : Observable<User>{
        const body = JSON.stringify({
                username: username,
                pokemon: []
            });
        const headers = {
            'X-API-Key': API_KEY,
            'Content-Type': 'application/json'
        }

        return this.http.post<User>(`${API_URL}/trainers`, body, { headers })
    }

    public authenticate(username : string, onSuccess: (user: User) => void){
        this.findByUsername(username)
            .pipe(
                switchMap((users: User[]) => {
                    if(users.length){
                        return of(users[0])
                    }

                    return this.createUser(username)
                })
            )
            .subscribe(
            (user: User) => {
                onSuccess(user)

            },
            () => {
                console.log("Error")
            }
        )
    }

}
