import {Injectable} from "@angular/core";
import {User} from "../../models/user.model";
import {Pokemon} from "../../models/pokemon.model";
import {of} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {PokeApiModel, PokeApiModelResults} from "../../models/poke-api.model";
import {colors} from "@angular/cli/utilities/color";
import {map, tap} from "rxjs/operators";
import {LoginService} from "../login/login.service";

const pokeAPIURL = environment.pokeAPIURL;
const pokeSpriteURL = environment.pokeSpriteURL;

const API_URL = environment.apiURL;
const API_KEY = environment.apiKey;

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    private _user: User | undefined


    constructor(private readonly http: HttpClient,
                private readonly loginService: LoginService) {
        const localUser = localStorage.getItem("user")

        if (localUser) {
            this.loginService.authenticate((JSON.parse(localUser) as User).username, (user) => {
                this.setUser(user)
            })
        }
    }


    get user(): User | undefined {
        return this._user
    }

    setUser(user: User | undefined): void {
        this._user = user
        localStorage.setItem("user", JSON.stringify(user))
        const body = JSON.stringify({
            pokemon: user?.pokemon
        });
        const headers = {
            'X-API-Key': API_KEY,
            'Content-Type': 'application/json'
        }
        this.http.patch<User>(`${API_URL}/trainers/${user?.id}`, body, { headers }).subscribe(
            (user: User) => {
                this._user = user
            }
        )
    }

    logout() {
        this._user = undefined
        localStorage.removeItem("user")
    }

    private requestPokemon(offset: number, limit: number) {
        return this.http.get<PokeApiModel>(`${pokeAPIURL}/pokemon?offset=${offset}&limit=${limit}`)
            .pipe(
                map((data) => {
                    return data.results.map(this.setPokemon.bind(this))
                }))
    }

    setPokemon(request: PokeApiModelResults) {
        const pokemonItem: Pokemon = {
            id: Number(request.url.split('/').slice(-2)[0]),
            name: request.name,
            url: request.url,
            image: `${pokeSpriteURL}/${request.url.split('/').slice(-2)[0]}.png`
        }
        return pokemonItem
    }

    get numberOfPokemon() {
        return this.http.get<PokeApiModel>(`${pokeAPIURL}/pokemon`).pipe(map((data) => data.count))
    }

    getPokemon(offset: number, limit: number) {
        return this.requestPokemon(offset, limit)

    }
}
