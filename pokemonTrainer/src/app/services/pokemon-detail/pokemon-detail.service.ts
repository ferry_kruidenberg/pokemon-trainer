import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {PokeApiModel, PokeApiModelResults} from "../../models/poke-api.model";
import {map} from "rxjs/operators";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Pokemon} from "../../models/pokemon.model";
import {PokemonTotalModel} from "../../models/pokemon-total.model";
import {observable} from "rxjs";
import {PokemonAbilityUrlModel} from "../../models/pokemon-ability-url.model";
import {PokemonFlavourTextModel} from "../../models/pokemon-flavour-text.model";

const pokeAPIURL = environment.pokeAPIURL;
const pokeSpriteURL = environment.pokeSpriteURL;


@Injectable({
    providedIn: 'root'
})
export class PokemonDetailService {

    private _pokemon: PokemonTotalModel | undefined;

    constructor(private readonly http: HttpClient) {

    }


    fetchPokemon(id: number) {
        return this.requestPokemon(id)
    }

    private requestPokemon(id: number) {
        return this.http.get<PokemonTotalModel>(`${pokeAPIURL}/pokemon/${id}`)
    }

    getAbilityText(url:string){
        return this.http.get<PokemonAbilityUrlModel>(url).pipe(
            map((ability) => {
                if (ability.effect_entries.length !== 0) {
                    return ability.effect_entries.filter((effect) => {
                        return effect.language.name === "en"
                    })[0].effect
                } else{
                    return "No description"
                }
            })
        )
    }

    getFlavourText(url:string){
        return this.http.get<PokemonFlavourTextModel>(url).pipe(
            map((flavourTextEntries) => {
                return flavourTextEntries.flavor_text_entries.filter((flavourText) =>{
                    return flavourText.language.name === "en"
                })[0]
            })
        )
    }

}
