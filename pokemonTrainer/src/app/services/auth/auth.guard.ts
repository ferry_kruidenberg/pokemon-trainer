import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {SessionService} from "../session/session.service";
import {User} from "../../models/user.model";
import {LoginService} from "../login/login.service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private readonly router: Router,
                private readonly sessionService: SessionService,
                private readonly loginService: LoginService) {
    }


    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        if (this.sessionService.user !== undefined) {
            return true
        } else if (localStorage.getItem("user")) {
            this.loginService.findByUsername((JSON.parse(localStorage.getItem("user")!) as User).username).subscribe(
                (users: User[]) => {

                    if (users.length) {
                        this.sessionService.setUser(users[0])
                        return true
                    } else {
                        this.router.navigate(['login'])
                        return false
                    }
                }
            )
        } else{
            this.router.navigate(['login'])
            return false
        }

        // this.router.navigate(['login'])
        return true
    }

}
