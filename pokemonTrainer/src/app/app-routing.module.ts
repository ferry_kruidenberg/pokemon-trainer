import { NgModule } from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginPage} from "./pages/login/login.page";
import {CataloguePage} from "./pages/catalogue/catalogue.page";
import {AuthGuard} from "./services/auth/auth.guard";
import {PokemonInfoPage} from "./pages/pokemon-info/pokemon-info.page";
import {ProfilePage} from "./pages/profile/profile.page";


export const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'catalogue/:id',
        pathMatch: 'full',
        component: CataloguePage,
        canActivate: [AuthGuard]
    },
    {
        path: `pokemon/:id`,
        component: PokemonInfoPage,
        canActivate: [AuthGuard]
    },
    {
        path: 'profile',
        component: ProfilePage,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full'
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
