# PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.7.

## Login
The user may enter a username to enter the website.<br/>
If the username does not exist, a new user is created instead.

## Catalogue
All Pokemon are displayed here.<br/>
The user may use the buttons at the bottom of the page to navigate through the pages.<br/>
Alternatively, the user may change the page number directly in the url.<br/>
On invalid entry, it should set the page back to the first page, or last page in case of higher numbers.<br/>
The user may click on a pokemon to go the the specific pokemon page.

## Profile
The user can see all pokemon they have in possession.<br/>
The user may click on a pokemon to go the the specific pokemon page.

## Pokemon Page
The user sees all the information about the specific pokemon.<br/>
If the user does not have this specific pokemon in their possession, they can capture them.<br/>
If the user does have this pokemon, they can choose to release them.
